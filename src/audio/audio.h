#pragma once
#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace pong
{
	namespace audio
	{
		extern Music gameplayMusic;
		extern Music menuMusic;
		extern Sound popSound;
		extern Sound optionSound;
		extern Sound scoreSound;

		void Init();
		void DeInit();
	}
}

#endif // !SOUND_H
