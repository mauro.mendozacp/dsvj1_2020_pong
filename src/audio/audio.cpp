#include "audio.h"

namespace pong
{
	namespace audio
	{
		Music gameplayMusic = LoadMusicStream("../res/assets/sounds/gameplay-soundtrack.mp3");
		Music menuMusic = LoadMusicStream("../res/assets/sounds/menu-soundtrack.mp3");
		Sound popSound = LoadSound("../res/assets/sounds/pop.ogg");
		Sound optionSound = LoadSound("../res/assets/sounds/menu-selection.ogg");
		Sound scoreSound = LoadSound("../res/assets/sounds/score.ogg");

		void Init()
		{
			InitAudioDevice();
			SetMusicVolume(gameplayMusic, 0.5f);
			SetMusicVolume(menuMusic, 0.5f);
			SetSoundVolume(popSound, 0.8f);
			SetSoundVolume(optionSound, 0.8f);
			SetSoundVolume(scoreSound, 0.8f);
		}

		void DeInit()
		{
			UnloadMusicStream(gameplayMusic);
			UnloadMusicStream(menuMusic);
			UnloadSound(popSound);
			UnloadSound(optionSound);
			UnloadSound(scoreSound);
		}
	}
}