#include "ball.h"
#include "audio/audio.h"

namespace pong
{
	namespace ball
	{
		float arrayExtraSpeeds[3] = { 0.75f, 1.0f, 1.25f };

		Ball::Ball()
		{
		}

		Ball::Ball(Vector2 position, float radius, int playerTouched, Color color, float speed, SPEED extraSpeed, bool collisionScreen, bool collisionPad, DIRECTION direction)
		{
			this->position = position;
			this->radius = radius;
			this->playerTouched = playerTouched;
			this->color = color;
			this->speed = speed;
			this->extraSpeed = extraSpeed;
			this->collisionScreen = collisionScreen;
			this->collisionPad = collisionPad;
			this->direction = direction;
		}

		Ball::~Ball()
		{
		}

		void Ball::Move()
		{
			float speedTotal = speed * arrayExtraSpeeds[((int)extraSpeed - 1)];

			switch (direction)
			{
			case DIRECTION::UP_RIGHT:
				position.x += speedTotal;
				position.y -= speedTotal;

				break;
			case DIRECTION::UP_LEFT:
				position.x -= speedTotal;
				position.y -= speedTotal;

				break;
			case DIRECTION::DOWN_RIGHT:
				position.x += speedTotal;
				position.y += speedTotal;

				break;
			case DIRECTION::DOWN_LEFT:
				position.x -= speedTotal;
				position.y += speedTotal;

				break;
			default:
				position.x = 0;
				position.y = 0;

				break;
			}
		}

		void Ball::Collision(int screenHeight, player::Player *player1, player::Player *player2)
		{
			if ((position.y - radius) <= 0)
			{
				if (!collisionScreen)
				{
					collisionScreen = true;
					ChangeDirection(SIDE::UP);
				}

				return;
			}

			if ((position.y + radius) >= screenHeight)
			{
				if (!collisionScreen)
				{
					collisionScreen = true;
					ChangeDirection(SIDE::DOWN);
				}

				return;
			}

			if (CheckCollisionCircleRec(position, radius, player1->GetRec()))
			{
				if (!collisionPad)
				{
					collisionPad = true;
					ChangeDirection(SIDE::LEFT);
					ChangeSpeed(player1->GetRec());
					SetColor(player1->GetColor());
				}

				return;
			}

			if (CheckCollisionCircleRec(position, radius, player2->GetRec()))
			{
				if (!collisionPad)
				{
					collisionPad = true;
					ChangeDirection(SIDE::RIGHT);
					ChangeSpeed(player2->GetRec());
					SetColor(player2->GetColor());
				}

				return;
			}

			collisionScreen = false;
			collisionPad = false;
		}

		void Ball::ChangeDirection(SIDE side)
		{
			switch (side)
			{
			case SIDE::UP:
				if (direction == DIRECTION::UP_RIGHT)
				{
					direction = DIRECTION::DOWN_RIGHT;
				}
				else
				{
					direction = DIRECTION::DOWN_LEFT;
				}

				break;
			case SIDE::RIGHT:
				if (direction == DIRECTION::UP_RIGHT)
				{
					direction = DIRECTION::UP_LEFT;
				}
				else
				{
					direction = DIRECTION::DOWN_LEFT;
				}

				break;
			case SIDE::DOWN:
				if (direction == DIRECTION::DOWN_RIGHT)
				{
					direction = DIRECTION::UP_RIGHT;
				}
				else
				{
					direction = DIRECTION::UP_LEFT;
				}

				break;
			case SIDE::LEFT:
				if (direction == DIRECTION::UP_LEFT)
				{
					direction = DIRECTION::UP_RIGHT;
				}
				else
				{
					direction = DIRECTION::DOWN_RIGHT;
				}

				break;
			default:
				direction = (DIRECTION)GetRandomValue(1, 4);
				break;
			}

			PlaySound(audio::popSound);
		}

		void Ball::Reset(Vector2 startPosition)
		{
			playerTouched = 0;
			extraSpeed = SPEED::NORMAL;
			position = startPosition;
			direction = (DIRECTION)GetRandomValue(1, 4);
			color = WHITE;
		}

		bool Ball::Goal(int screenWidth, player::Player *player1, player::Player *player2)
		{
			if ((position.x - radius) <= 0)
			{
				player2->SetScore(player2->GetScore() + 1);

				return true;
			}

			if (position.x + radius >= screenWidth)
			{
				player1->SetScore(player1->GetScore() + 1);

				return true;
			}

			return false;
		}

		void Ball::ChangeSpeed(Rectangle recPlayer)
		{
			Rectangle auxRec1, auxRec2;

			auxRec1.x = recPlayer.x;
			auxRec1.y = recPlayer.y;
			auxRec1.height = recPlayer.height / 6;
			auxRec1.width = recPlayer.width;

			auxRec2.x = recPlayer.x;
			auxRec2.y = recPlayer.y + recPlayer.height * 5 / 6;
			auxRec2.height = recPlayer.height / 6;
			auxRec2.width = recPlayer.width;

			if (CheckCollisionCircleRec(position, radius, auxRec1) || CheckCollisionCircleRec(position, radius, auxRec2))
			{
				extraSpeed = SPEED::FAST;
			}
			else
			{
				auxRec1.x = recPlayer.x;
				auxRec1.y = recPlayer.y + recPlayer.height * 5 / 12;
				auxRec1.height = recPlayer.height / 6;
				auxRec1.width = recPlayer.width;

				if (CheckCollisionCircleRec(position, radius, auxRec1))
				{
					extraSpeed = SPEED::SLOW;
				}
				else
				{
					extraSpeed = SPEED::NORMAL;
				}
			}
		}

		void Ball::ShowVisualBall()
		{
			DrawCircle((int)position.x, (int)position.y, radius, color);
		}

		Vector2 Ball::GetPosition()
		{
			return this->position;
		}

		void Ball::SetPosition(Vector2 position)
		{
			this->position = position;
		}

		float Ball::GetRadius()
		{
			return this->radius;
		}

		void Ball::SetRadius(float radius)
		{
			this->radius = radius;
		}

		int Ball::GetPlayerTouched()
		{
			return this->playerTouched;
		}

		void Ball::SetPlayerTouched(int playerTouched)
		{
			this->playerTouched = playerTouched;
		}

		Color Ball::GetColor()
		{
			return this->color;
		}

		void Ball::SetColor(Color color)
		{
			this->color = color;
		}

		float Ball::GetSpeed()
		{
			return this->speed;
		}

		void Ball::SetSpeed(float speed)
		{
			this->speed = speed;
		}

		SPEED Ball::GetExtraSpeed()
		{
			return this->extraSpeed;
		}

		void Ball::SetExtraSpeed(SPEED extraSpeed)
		{
			this->extraSpeed = extraSpeed;
		}

		bool Ball::GetCollisionScreen()
		{
			return this->collisionScreen;
		}

		void Ball::SetCollisionScreen(bool collisionScreen)
		{
			this->collisionScreen = collisionScreen;
		}

		bool Ball::GetCollisionPad()
		{
			return this->collisionPad;
		}

		void Ball::SetCollisionPad(bool collisionPad)
		{
			this->collisionPad = collisionPad;
		}

		DIRECTION Ball::GetDirection()
		{
			return this->direction;
		}

		void Ball::SetDirection(DIRECTION direction)
		{
			this->direction = direction;
		}
	}
}