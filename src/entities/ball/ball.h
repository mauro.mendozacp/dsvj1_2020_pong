#pragma once
#ifndef BALL_H
#define BALL_H

#include "raylib.h"
#include "entities\player\player.h"

namespace pong
{
	namespace ball
	{
		enum class SPEED
		{
			SLOW = 1,
			NORMAL,
			FAST
		};

		enum class SIDE
		{
			UP = 1,
			RIGHT,
			DOWN,
			LEFT
		};

		enum class DIRECTION
		{
			UP_RIGHT = 1,
			UP_LEFT,
			DOWN_RIGHT,
			DOWN_LEFT
		};

		class Ball
		{
		public:
			Ball();
			Ball(Vector2 position, float radius, int playerTouched, Color color, float speed, SPEED extraSpeed, bool collisionScreen, bool collisionPad, DIRECTION direction);
			~Ball();

			void Move();
			void Collision(int screenHeight, player::Player *player1, player::Player *player2);
			void ChangeDirection(SIDE side);
			void Reset(Vector2 startPosition);
			bool Goal(int screenWidth, player::Player *player1, player::Player *player2);
			void ChangeSpeed(Rectangle recPlayer);
			void ShowVisualBall();

			Vector2 GetPosition();
			void SetPosition(Vector2 position);
			float GetRadius();
			void SetRadius(float radius);
			int GetPlayerTouched();
			void SetPlayerTouched(int playerTouched);
			Color GetColor();
			void SetColor(Color color);
			float GetSpeed();
			void SetSpeed(float speed);
			SPEED GetExtraSpeed();
			void SetExtraSpeed(SPEED extraSpeed);
			bool GetCollisionScreen();
			void SetCollisionScreen(bool collisionScreen);
			bool GetCollisionPad();
			void SetCollisionPad(bool collisionPad);
			DIRECTION GetDirection();
			void SetDirection(DIRECTION direction);

		private:
			Vector2 position;
			float radius;
			int playerTouched;
			Color color;
			float speed;
			SPEED extraSpeed;
			bool collisionScreen;
			bool collisionPad;
			DIRECTION direction;

		};
	}
}

#endif // !BAL_H