#include "item.h"
#include "scenes/game_setting/game_setting.h"

namespace pong
{
	namespace item
	{
		const int itemWidth = 20;
		const int itemHeight = 20;

		const int itemLenght = 5;

		//Items
		const int obstacleLenght = 2;
		Rectangle obstacle[obstacleLenght];
		const float obstaclePaddleWidth = 20.0f;
		const float obstaclePaddleHeight = 80.0f;
		Rectangle shield;

		Item::Item()
		{
		}

		Item::Item(Rectangle rec, int frameCounter, bool inGame, bool actived, ItemPower itemPower)
		{
			this->rec = rec;
			this->frameCounter = frameCounter;
			this->inGame = inGame;
			this->actived = actived;
			this->itemPower = itemPower;
		}

		Item::~Item()
		{
		}

		bool Item::PutItem(int coolDown)
		{
			if (!inGame && !actived)
			{
				frameCounter++;
				if (((frameCounter / coolDown) % 2) == 1)
				{
					inGame = true;
					frameCounter = 0;

					return true;
				}
			}

			return false;
		}

		void Item::StartItemValues(int screenHeight, int screenWidth)
		{
			itemPower = (ItemPower)GetRandomValue(1, item::itemLenght);
			rec.x = (float)GetRandomValue((screenWidth / 8), (screenWidth * 6 / 8));
			rec.y = (float)GetRandomValue((screenHeight / 8), (screenHeight * 6 / 8));
		}

		bool Item::Disable(int duration)
		{
			if (actived)
			{
				frameCounter++;
				if (((frameCounter / duration) % 2) == 1 && IsPowerForTime())
				{
					actived = false;
					frameCounter = 0;

					return true;
				}
			}

			return false;
		}

		bool Item::IsPowerForTime()
		{
			switch (itemPower)
			{
			case ItemPower::MULTI_BALLS:
			case ItemPower::OBSTACLES:
			case ItemPower::INVERT_SPEED:
			case ItemPower::INVERT_CONTROLS:
				return true;

			case ItemPower::SHIELD:
			case ItemPower::INCREASED_PADDLE:
			case ItemPower::INCREASED_SPEED:
			default:
				return false;
			}
		}

		bool Item::CollisionBall(ball::Ball *ball)
		{
			if (inGame && CheckCollisionCircleRec(ball->GetPosition(), ball->GetRadius(), rec))
			{
				actived = true;
				inGame = false;

				return true;
			}

			return false;
		}

		void Item::ActivePower(player::Player *player1, player::Player *player2, ball::Ball *ball, int screenWidth, int screenHeight)
		{
			Rectangle auxRec;
			int auxInput = 0;

			switch (itemPower)
			{
			case ItemPower::SHIELD:
				if (ColorToInt(ball->GetColor()) == ColorToInt(player1->GetColor()))
				{
					item::shield.x = player1->GetRec().x + player1->GetRec().width - 10;
					item::shield.y = 0;
					item::shield.height = (float)screenHeight;
					item::shield.width = 10;
				}
				else
				{
					item::shield.x = player2->GetRec().x;
					item::shield.y = 0;
					item::shield.height = (float)screenHeight;
					item::shield.width = 10;
				}
				break;
			case ItemPower::INCREASED_PADDLE:
				if (ColorToInt(ball->GetColor()) == ColorToInt(player1->GetColor()))
				{
					auxRec.x = player1->GetRec().x;
					auxRec.y = player1->GetRec().y;
					auxRec.height = player::playerPaddleHeight + (player::playerPaddleHeight / 100 * 50);
					auxRec.width = player::playerPaddleWidth;

					if ((auxRec.y + auxRec.height) >= screenHeight)
					{
						auxRec.y = screenHeight - auxRec.height;
					}

					player1->SetRec(auxRec);
				}
				else
				{
					auxRec.x = player2->GetRec().x;
					auxRec.y = player2->GetRec().y;
					auxRec.height = player::playerPaddleHeight + (player::playerPaddleHeight / 100 * 25);
					auxRec.width = player::playerPaddleWidth;

					if ((auxRec.y + auxRec.height) >= screenHeight)
					{
						auxRec.y = screenHeight - auxRec.height;
					}

					player2->SetRec(auxRec);
				}

				break;
			case ItemPower::INCREASED_SPEED:
				if (ColorToInt(ball->GetColor()) == ColorToInt(player1->GetColor()))
				{
					player1->SetSpeed(game_setting::speedPaddle * 2);
				}
				else
				{
					player2->SetSpeed(game_setting::speedPaddle * 2);
				}

				break;
			case ItemPower::OBSTACLES:
				item::obstacle[0].x = ((screenWidth / 2) - (item::obstaclePaddleWidth / 2));
				item::obstacle[0].y = ((screenHeight / 4) - (item::obstaclePaddleHeight / 2));
				item::obstacle[0].width = item::obstaclePaddleWidth;
				item::obstacle[0].height = item::obstaclePaddleHeight;

				item::obstacle[1].x = ((screenWidth / 2) - (item::obstaclePaddleWidth / 2));
				item::obstacle[1].y = ((screenHeight * 3 / 4) - (item::obstaclePaddleHeight / 2));
				item::obstacle[1].width = item::obstaclePaddleWidth;
				item::obstacle[1].height = item::obstaclePaddleHeight;

				break;
			case ItemPower::INVERT_CONTROLS:
				auxInput = input::moveUpPlayer1;
				input::moveUpPlayer1 = input::moveDownPlayer1;
				input::moveDownPlayer1 = auxInput;

				auxInput = input::moveUpPlayer2;
				input::moveUpPlayer2 = input::moveDownPlayer2;
				input::moveDownPlayer2 = auxInput;

				break;
			case ItemPower::INVERT_SPEED:
				break;
			case ItemPower::MULTI_BALLS:
				break;
			default:
				break;
			}
		}

		void Item::DisablePower(player::Player *player1, player::Player *player2)
		{
			Rectangle auxRec;
			int auxInput = 0;

			switch (itemPower)
			{
			case ItemPower::SHIELD:
				break;
			case ItemPower::INCREASED_PADDLE:
				auxRec.x = player1->GetRec().x;
				auxRec.y = player1->GetRec().y;
				auxRec.height = player::playerPaddleHeight;
				auxRec.width = player1->GetRec().width;

				player1->SetRec(auxRec);

				auxRec.x = player2->GetRec().x;
				auxRec.y = player2->GetRec().y;
				auxRec.height = player::playerPaddleHeight;
				auxRec.width = player2->GetRec().width;

				player2->SetRec(auxRec);

				break;
			case ItemPower::INCREASED_SPEED:
				player1->SetSpeed(game_setting::speedPaddle);
				player2->SetSpeed(game_setting::speedPaddle);

				break;
			case ItemPower::OBSTACLES:
				break;
			case ItemPower::INVERT_CONTROLS:
				auxInput = input::moveUpPlayer1;
				input::moveUpPlayer1 = input::moveDownPlayer1;
				input::moveDownPlayer1 = auxInput;

				auxInput = input::moveUpPlayer2;
				input::moveUpPlayer2 = input::moveDownPlayer2;
				input::moveDownPlayer2 = auxInput;

				break;
			case ItemPower::INVERT_SPEED:
				break;
			case ItemPower::MULTI_BALLS:
				break;
			default:
				break;
			}
		}

		void Item::LoopPower(player::Player *player1, player::Player *player2, ball::Ball *ball)
		{
			if (actived)
			{
				switch (itemPower)
				{
				case ItemPower::SHIELD:
					if (CheckCollisionCircleRec(ball->GetPosition(), ball->GetRadius(), item::shield))
					{
						if (!ball->GetCollisionPad())
						{
							if (ColorToInt(ball->GetColor()) == ColorToInt(player1->GetColor()))
							{
								ball->SetCollisionPad(true);
								ball->ChangeDirection(ball::SIDE::RIGHT);
								ball->SetColor(player2->GetColor());
							}
							else
							{
								ball->SetCollisionPad(true);
								ball->ChangeDirection(ball::SIDE::LEFT);
								ball->SetColor(player1->GetColor());
							}
						}
						actived = false;
						DisablePower(player1, player2);
					}

					break;
				case ItemPower::INCREASED_PADDLE:
					break;
				case ItemPower::INCREASED_SPEED:
					break;
				case ItemPower::OBSTACLES:
					if (CheckCollisionCircleRec(ball->GetPosition(), ball->GetRadius(), item::obstacle[0]))
					{
						if (!ball->GetCollisionPad())
						{
							if (ColorToInt(ball->GetColor()) == ColorToInt(player1->GetColor()))
							{
								ball->SetCollisionPad(true);
								ball->ChangeDirection(ball::SIDE::RIGHT);
							}
							else
							{
								ball->SetCollisionPad(true);
								ball->ChangeDirection(ball::SIDE::LEFT);
							}
						}
					}
					if (CheckCollisionCircleRec(ball->GetPosition(), ball->GetRadius(), item::obstacle[1]))
					{
						if (!ball->GetCollisionPad())
						{
							if (ColorToInt(ball->GetColor()) == ColorToInt(player1->GetColor()))
							{
								ball->SetCollisionPad(true);
								ball->ChangeDirection(ball::SIDE::RIGHT);
							}
							else
							{
								ball->SetCollisionPad(true);
								ball->ChangeDirection(ball::SIDE::LEFT);
							}
						}
					}

					break;
				case ItemPower::INVERT_CONTROLS:
					break;
				case ItemPower::INVERT_SPEED:
					break;
				case ItemPower::MULTI_BALLS:
					break;
				default:
					break;
				}
			}
		}

		void Item::ShowVisualPower()
		{
			if (actived)
			{
				switch (itemPower)
				{
				case ItemPower::SHIELD:
					DrawRectangle((int)item::shield.x, (int)item::shield.y, (int)item::shield.width, (int)item::shield.height, WHITE);
					break;
				case ItemPower::INCREASED_PADDLE:
					break;
				case ItemPower::INCREASED_SPEED:
					break;
				case ItemPower::OBSTACLES:
					DrawRectangle((int)item::obstacle[0].x, (int)item::obstacle[0].y, (int)item::obstacle[0].width, (int)item::obstacle[0].height, WHITE);
					DrawRectangle((int)item::obstacle[1].x, (int)item::obstacle[1].y, (int)item::obstacle[1].width, (int)item::obstacle[1].height, WHITE);
					break;
				case ItemPower::INVERT_CONTROLS:
					break;
				case ItemPower::INVERT_SPEED:
					break;
				case ItemPower::MULTI_BALLS:
					break;
				default:
					break;
				}
			}
		}

		void Item::ShowVisualPowerDescription(int screenWidth, int screenHeight, int powerDescDuration)
		{
			if ((frameCounter < powerDescDuration) && actived)
			{
				const char* powerText;
				int font = 30;
				Color color = LIGHTGRAY;

				switch (itemPower)
				{
				case ItemPower::SHIELD:
					powerText = "SHIELD";
					break;
				case ItemPower::INCREASED_PADDLE:
					powerText = "INCREASED PADDLE";
					break;
				case ItemPower::INCREASED_SPEED:
					powerText = "INCREASED SPEED";
					break;
				case ItemPower::OBSTACLES:
					powerText = "OBSTACLES";
					break;
				case ItemPower::INVERT_CONTROLS:
					powerText = "INVERT CONTROLS";
					break;
				case ItemPower::INVERT_SPEED:
					powerText = "INVERT SPEED";
					break;
				case ItemPower::MULTI_BALLS:
					powerText = "MULTI BALLS";
					break;
				default:
					powerText = " ";
					break;
				}

				DrawText(powerText, ((screenWidth / 2) - (MeasureText(powerText, font) / 2)), ((screenHeight / 2) - (font / 2)), font, color);
			}
		}

		void Item::ShowVisualItem()
		{
			if (inGame)
			{
				const char* text;
				int posX = 0;
				int posY = 0;
				int font = 14;
				Color color = WHITE;

				DrawRectangleLines((int)rec.x, (int)rec.y, (int)rec.width, (int)rec.height, color);

				switch (itemPower)
				{
				case ItemPower::SHIELD:
					text = "S";
					posX = (((int)rec.x + (int)rec.width / 2) - (MeasureText(text, font) / 2));
					posY = (((int)rec.y + (int)rec.height / 2) - (font / 2));

					DrawText(text, posX, posY, font, color);

					break;
				case ItemPower::INCREASED_PADDLE:
					text = "+|";
					posX = (((int)rec.x + (int)rec.width / 2) - (MeasureText(text, font) / 2));
					posY = (((int)rec.y + (int)rec.height / 2) - (font / 2));

					DrawText(text, posX, posY, font, color);

					break;
				case ItemPower::INCREASED_SPEED:
					text = "x2";
					posX = (((int)rec.x + (int)rec.width / 2) - (MeasureText(text, font) / 2));
					posY = (((int)rec.y + (int)rec.height / 2) - (font / 2));

					DrawText(text, posX, posY, font, color);

					break;
				case ItemPower::OBSTACLES:
					text = "O";
					posX = (((int)rec.x + (int)rec.width / 2) - (MeasureText(text, font) / 2));
					posY = (((int)rec.y + (int)rec.height / 2) - (font / 2));

					DrawText(text, posX, posY, font, color);

					break;
				case ItemPower::INVERT_CONTROLS:
					text = "IC";
					posX = (((int)rec.x + (int)rec.width / 2) - (MeasureText(text, font) / 2));
					posY = (((int)rec.y + (int)rec.height / 2) - (font / 2));

					DrawText(text, posX, posY, font, color);

					break;
				case ItemPower::INVERT_SPEED:
					break;
				case ItemPower::MULTI_BALLS:
					break;
				default:
					break;
				}
			}
		}

		void Item::Reset()
		{
			inGame = false;
			actived = false;
			rec.x = 0;
			rec.y = 0;
			frameCounter = 0;
		}

		Rectangle Item::GetRec()
		{
			return this->rec;
		}

		void Item::SetRec(Rectangle rec)
		{
			this->rec = rec;
		}

		int Item::GetFrameCounter()
		{
			return this->frameCounter;
		}

		void Item::SetFrameCounter(int frameCounter)
		{
			this->frameCounter = frameCounter;
		}

		bool Item::GetInGame()
		{
			return this->inGame;
		}

		void Item::SetInGame(bool inGame)
		{
			this->inGame = inGame;
		}

		bool Item::GetActived()
		{
			return this->actived;
		}

		void Item::SetActived(bool actived)
		{
			this->actived = actived;
		}

		ItemPower Item::GetItemPower()
		{
			return this->itemPower;
		}

		void Item::SetItemPower(ItemPower itemPower)
		{
			this->itemPower = itemPower;
		}
	}
}