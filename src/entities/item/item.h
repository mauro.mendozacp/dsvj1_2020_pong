#pragma once
#ifndef ITEM_H
#define ITEM_H

#include "raylib.h"
#include "entities\ball\ball.h"
#include "entities\player\player.h"
#include "input\input.h"

namespace pong
{
	namespace item
	{
		extern const int itemWidth;
		extern const int itemHeight;

		extern const int itemLenght;

		extern const int obstacleLenght;
		extern Rectangle obstacle[];
		extern const float obstaclePaddleWidth;
		extern const float obstaclePaddleHeight;
		extern Rectangle shield;

		enum class ItemPower
		{
			SHIELD = 1,
			INCREASED_PADDLE,
			INCREASED_SPEED,
			OBSTACLES,
			INVERT_CONTROLS,
			INVERT_SPEED,
			MULTI_BALLS
		};

		class Item
		{
		public:
			Item();
			Item(Rectangle rec, int frameCounter, bool inGame, bool actived, ItemPower itemPower);
			~Item();
			bool PutItem(int coolDown);
			void StartItemValues(int screenHeight, int screenWidth);
			bool Disable(int duration);
			bool CollisionBall(ball::Ball *ball);
			void ActivePower(player::Player *player1, player::Player *player2, ball::Ball *ball, int screenWidth, int screenHeight);
			void LoopPower(player::Player *player1, player::Player *player2, ball::Ball *ball);
			void DisablePower(player::Player *player1, player::Player *player2);
			bool IsPowerForTime();
			void ShowVisualItem();
			void ShowVisualPower();
			void ShowVisualPowerDescription(int screenWidth, int screenHeight, int powerDescDuration);
			void Reset();

			Rectangle GetRec();
			void SetRec(Rectangle rec);
			int GetFrameCounter();
			void SetFrameCounter(int frameCounter);
			bool GetInGame();
			void SetInGame(bool inGame);
			bool GetActived();
			void SetActived(bool actived);
			ItemPower GetItemPower();
			void SetItemPower(ItemPower itemPower);

		private:
			Rectangle rec;
			int frameCounter;
			bool inGame;
			bool actived;
			ItemPower itemPower;

		};
	}
}

#endif // !ITEM_H