#pragma once
#ifndef OPTION_H
#define OPTION_H

#include "raylib.h"
#include "input\input.h"

namespace pong
{
	namespace option
	{
		class Option
		{
		public:
			Option();
			Option(Vector2 position, int option, int speed, char caracter);
			~Option();

			void Move(int newOption);
			int GetNewOption();
			void Reset(Vector2 optionStartPosition);
			void ShowVisualOption();

			Vector2 GetPosition();
			void SetPosition(Vector2 position);
			int GetOption();
			void SetOption(int option);
			int GetSpeed();
			void SetSpeed(int speed);
			char GetCaracter();
			void SetCaracter(char caracter);

		private:
			Vector2 position;
			int optionNumber;
			int speed;
			char caracter;

		};
	}
}

#endif // !OPTION_H