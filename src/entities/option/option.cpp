#include "option.h"
#include "audio/audio.h"

namespace pong
{
	namespace option
	{
		Option::Option()
		{
		}

		Option::Option(Vector2 position, int option, int speed, char caracter)
		{
			this->position = position;
			this->caracter = caracter;
			this->speed = speed;
			this->optionNumber = option;
		}

		Option::~Option()
		{
		}

		void Option::Move(int newOption)
		{
			this->optionNumber += newOption;
			this->position.y += this->speed * newOption;
			PlaySound(audio::optionSound);
		}

		int Option::GetNewOption()
		{
			if (IsKeyPressed(input::moveUp))
			{
				return -1;
			}
			if (IsKeyPressed(input::moveDown))
			{
				return 1;
			}

			return 0;
		}

		void Option::ShowVisualOption()
		{
			DrawText(TextFormat("%c", caracter), (int)position.x, (int)position.y, 20, LIGHTGRAY);
		}

		void Option::Reset(Vector2 optionStartPosition)
		{
			this->position = optionStartPosition;
			this->optionNumber = 1;
		}

		Vector2 Option::GetPosition()
		{
			return this->position;
		}

		void Option::SetPosition(Vector2 position)
		{
			this->position = position;
		}

		int Option::GetOption()
		{
			return this->optionNumber;
		}

		void Option::SetOption(int option)
		{
			this->optionNumber = option;
		}

		int Option::GetSpeed()
		{
			return this->speed;
		}

		void Option::SetSpeed(int speed)
		{
			this->speed = speed;
		}

		char Option::GetCaracter()
		{
			return this->caracter;
		}

		void Option::SetCaracter(char caracter)
		{
			this->caracter = caracter;
		}
	}
}