#include "player.h"

namespace pong
{
	namespace player
	{
		const float playerPaddleWidth = 20.0f;
		const float playerPaddleHeight = 120.0f;

		Player::Player()
		{
		}

		Player::Player(Rectangle rec, Color color, float speed, int score, int roundsWon, int roundsLost)
		{
			this->rec = rec;
			this->color = color;
			this->speed = speed;
			this->score = score;
			this->roundsWon = roundsWon;
			this->roundsLost = roundsLost;
		}

		Player::~Player()
		{
		}

		float Player::GetPositionY(MOVE move)
		{
			switch (move)
			{
			case MOVE::UP:
				return (rec.y + (speed * -1.0f));
			case MOVE::DOWN:
				return (rec.y + (speed * 1.0f));
			}

			return 0;
		}

		void Player::Move(int screenHeight, MOVE move)
		{
			float newPositionY = GetPositionY(move);

			if (CheckPositionY(screenHeight, newPositionY))
			{
				rec.y = newPositionY;
			}
		}

		bool Player::CheckPositionY(int screenHeight, float PosY)
		{
			if ((PosY) > 0 && (PosY + rec.height) < screenHeight)
			{
				return true;
			}

			return false;
		}

		void Player::ResetPosition(Vector2 startPosition)
		{
			rec.x = startPosition.x;
			rec.y = startPosition.y;
		}

		void Player::ResetRound(Vector2 startPosition)
		{
			ResetPosition(startPosition);
			score = 0;
		}

		void Player::Start(Vector2 startPosition)
		{
			ResetRound(startPosition);
			roundsWon = 0;
			roundsLost = 0;
		}

		void Player::ShowVisualPlayer()
		{
			DrawRectangle((int)rec.x, (int)rec.y, (int)rec.width, (int)rec.height, color);
		}

		Rectangle Player::GetRec()
		{
			return this->rec;
		}

		void Player::SetRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Color Player::GetColor()
		{
			return this->color;
		}

		void Player::SetColor(Color color)
		{
			this->color = color;
		}

		float Player::GetSpeed()
		{
			return this->speed;
		}

		void Player::SetSpeed(float speed)
		{
			this->speed = speed;
		}

		int Player::GetScore()
		{
			return this->score;
		}

		void Player::SetScore(int score)
		{
			this->score = score;
		}

		int Player::GetRoundsWon()
		{
			return this->roundsWon;
		}

		void Player::SetRoundsWon(int roundsWon)
		{
			this->roundsWon = roundsWon;
		}

		int Player::GetRoundsLost()
		{
			return this->roundsLost;
		}

		void Player::SetRoundsLost(int roundsLost)
		{
			this->roundsLost = roundsLost;
		}
	}
}