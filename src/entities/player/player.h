#pragma once
#include "raylib.h"

namespace pong
{
	namespace player
	{
		extern const float playerPaddleWidth;
		extern const float playerPaddleHeight;

		enum class MOVE
		{
			UP = 1,
			DOWN
		};

		class Player
		{
		public:
			Player();
			Player(Rectangle rec, Color color, float speed, int score, int roundsWon, int roundsLost);
			~Player();

			float GetPositionY(MOVE move);
			void Move(int screenHeight, MOVE move);
			bool CheckPositionY(int screenHeight, float PosY);
			void ResetPosition(Vector2 startPosition);
			void ResetRound(Vector2 startPosition);
			void Start(Vector2 startPosition);
			void ShowVisualPlayer();

			Rectangle GetRec();
			void SetRec(Rectangle rec);
			Color GetColor();
			void SetColor(Color color);
			float GetSpeed();
			void SetSpeed(float speed);
			int GetScore();
			void SetScore(int score);
			int GetRoundsWon();
			void SetRoundsWon(int roundsWon);
			int GetRoundsLost();
			void SetRoundsLost(int roundsLost);

		private:
			Rectangle rec;
			Color color;
			float speed;
			int score;
			int roundsWon;
			int roundsLost;

		};
	}
}