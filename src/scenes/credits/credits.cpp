#include "credits.h"
#include "game/game.h"

namespace pong
{
	namespace credits
	{
		void Init()
		{

		}

		void Update()
		{
			if(input::CheckAccept())
			{
				DeInit();
				main_menu::Init();
				game::gameStatus = game::GAME::MAIN_MENU;
			}

			UpdateMusicStream(audio::menuMusic);
		}

		void Draw()
		{
			const char* text = "GAME DEVELOPER - MAURO MENDOZA";
			int posX = (game::screenWidth / 2);
			int posY = (game::screenHeight / 4);
			int font = 35;
			Color color = LIGHTGRAY;

			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, LIGHTGRAY);

			posY = (game::screenHeight / 2);
			font = 25;
			int spacing = font + 10;

			text = "Menu Music: (Geometry Dash - Menu Song Mix Megalovania)";
			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, LIGHTGRAY);

			text = "GamePlay Music: (Bossfight - Milky Ways)";
			DrawText(text, (posX - (MeasureText(text, font) / 2)), ((posY + spacing) - (font / 2)), font, LIGHTGRAY);

			text = "ENTER FOR BACK TO MENU";
			posY = (game::screenHeight * 3 / 4);
			font = 20;

			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, LIGHTGRAY);
		}

		void DeInit()
		{
		}
	}
}