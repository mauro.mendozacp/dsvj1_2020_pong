#pragma once
#ifndef CREDITS_H
#define CREDITS_H

#include "raylib.h"

namespace pong
{
	namespace credits
	{
		void Init();
		void Update();
		void Draw();
		void DeInit();
	}
}

#endif // !CREDITS_H