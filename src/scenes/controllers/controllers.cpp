#include "controllers.h"
#include "game/game.h"

namespace pong
{
	namespace controllers
	{
		option::Option *option;
		const Vector2 optionStartPosition = { (float)(game::screenWidth / 8), (float)(game::screenHeight / 2) };
		bool wait = false;

		void Init()
		{
			option = new option::Option(optionStartPosition, (int)CONTROLS::MOVE_UP_PLAYER1, 30, char(62));
			wait = false;
		}

		void Update()
		{
			if (!wait)
			{
				MoveOption();
			}
			if (input::CheckAccept())
			{
				if ((CONTROLS)option->GetOption() != CONTROLS::GO_BACK)
				{
					wait = true;
				}
				else
				{
					DeInit();
					main_menu::Init();
					game::gameStatus = game::GAME::MAIN_MENU;
				}
			}
			if (wait)
			{
				input::inputKey = input::GetInputChange();

				switch ((CONTROLS)option->GetOption())
				{
				case CONTROLS::MOVE_UP_PLAYER1:
					input::moveUpPlayer1 = input::inputKey;
					break;
				case CONTROLS::MOVE_DOWN_PLAYER1:
					input::moveDownPlayer1 = input::inputKey;
					break;
				case CONTROLS::MOVE_UP_PLAYER2:
					input::moveUpPlayer2 = input::inputKey;
					break;
				case CONTROLS::MOVE_DOWN_PLAYER2:
					input::moveDownPlayer2 = input::inputKey;
					break;
				case CONTROLS::PAUSE:
					input::pause = input::inputKey;
					break;

				default:
					break;
				}

				if (input::inputKey != 0)
				{
					wait = false;
				}
			}

			UpdateMusicStream(audio::menuMusic);
		}

		void Draw()
		{
			const char* text = "CONTROLS";
			int posX = (game::screenWidth / 2);
			int posY = (game::screenHeight / 4);
			int font = 35;
			Color color = LIGHTGRAY;

			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);

			option->ShowVisualOption();
			font = 20;
			posX = (int)optionStartPosition.x + font;
			posY = (int)optionStartPosition.y;

			DrawText("MOVE UP PLAYER 1", posX, posY, font, color);
			DrawText("MOVE DOWN PLAYER 1", posX, (posY + option->GetSpeed()), font, color);
			DrawText("MOVE UP PLAYER 2", posX, (posY + (option->GetSpeed() * 2)), font, color);
			DrawText("MOVE DOWN PLAYER 2", posX, (posY + (option->GetSpeed() * 3)), font, color);
			DrawText("PAUSE GAME", posX, (posY + (option->GetSpeed() * 4)), font, color);
			DrawText("BACK TO MENU", posX, (posY + (option->GetSpeed() * 5)), font, color);

			posX = (game::screenWidth * 3 / 4);
			posY = (int)optionStartPosition.y;

			DrawText(TextFormat("%s", input::GetInputText(input::moveUpPlayer1)), (posX - (MeasureText(TextFormat("%s", input::GetInputText(input::moveUpPlayer1)), font) / 2)), posY, font, color);
			DrawText(TextFormat("%s", input::GetInputText(input::moveDownPlayer1)), (posX - (MeasureText(TextFormat("%s", input::GetInputText(input::moveDownPlayer1)), font) / 2)), (posY + option->GetSpeed()), font, color);
			DrawText(TextFormat("%s", input::GetInputText(input::moveUpPlayer2)), (posX - (MeasureText(TextFormat("%s", input::GetInputText(input::moveUpPlayer2)), font) / 2)), (posY + (option->GetSpeed() * 2)), font, color);
			DrawText(TextFormat("%s", input::GetInputText(input::moveDownPlayer2)), (posX - (MeasureText(TextFormat("%s", input::GetInputText(input::moveDownPlayer2)), font) / 2)), (posY + (option->GetSpeed() * 3)), font, color);
			DrawText(TextFormat("%s", input::GetInputText(input::pause)), (posX - (MeasureText(TextFormat("%s", input::GetInputText(input::pause)), font) / 2)), (posY + (option->GetSpeed() * 4)), font, color);
		}

		void DeInit()
		{
			delete option;
		}

		void MoveOption()
		{
			if (input::CheckMoveUpDown())
			{
				int auxOption = option->GetNewOption();

				if (CheckOption(auxOption))
				{
					option->Move(auxOption);
				}
			}
		}

		bool CheckOption(int iOption)
		{
			switch ((CONTROLS)(option->GetOption() + iOption))
			{
			case CONTROLS::MOVE_UP_PLAYER1:
			case CONTROLS::MOVE_DOWN_PLAYER1:
			case CONTROLS::MOVE_UP_PLAYER2:
			case CONTROLS::MOVE_DOWN_PLAYER2:
			case CONTROLS::PAUSE:
			case CONTROLS::GO_BACK:
				return true;
			default:
				return false;
			}

			return false;
		}
	}
}