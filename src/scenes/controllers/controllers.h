#pragma once
#ifndef CONTROLLERS_H
#define CONTROLLERS_H

#include "raylib.h"
#include "entities/option/option.h"

namespace pong
{
	namespace controllers
	{
		enum class CONTROLS
		{
			MOVE_UP_PLAYER1 = 1,
			MOVE_DOWN_PLAYER1,
			MOVE_UP_PLAYER2,
			MOVE_DOWN_PLAYER2,
			PAUSE,
			GO_BACK
		};

		extern option::Option *option;
		extern const Vector2 optionStartPosition;
		extern bool wait;

		void Init();
		void Update();
		void Draw();
		void DeInit();

		void MoveOption();
		bool CheckOption(int iOption);
	}
}

#endif // !CONTROLLERS_H
