#include "gameplay.h"
#include "game/game.h"

namespace pong
{
	namespace gameplay
	{
		Vector2 player1StartPosition = { 0, 0 };
		Vector2 player2StartPosition = { 0, 0 };
		Vector2 ballStartPosition = { 0, 0 };

		player::Player *player1;
		player::Player *player2;
		ball::Ball *ball;
		item::Item *item;

		int itemCoolDown = 0;
		int itemDuration = 0;
		int powerDescriptionDuration = 0;

		bool startGame = false;
		int frameCounter = 0;
		GAME_MODE gameMode;

		void Init(GAME_MODE gm)
		{
			const float ballRadiusCircle = 15;

			player1StartPosition = { ((float)game::screenWidth / 16), ((float)game::screenHeight / 2 - player::playerPaddleHeight / 2) };
			player2StartPosition = { ((float)game::screenWidth - (float)game::screenWidth / 16), ((float)game::screenHeight / 2 - player::playerPaddleHeight / 2) };
			ballStartPosition = { ((float)game::screenWidth / 2.0f), ((float)game::screenHeight / 2) };

			itemCoolDown = game::frames * 5;
			itemDuration = game::frames * 8;
			powerDescriptionDuration = game::frames * 2;

			startGame = false;
			frameCounter = 0;
			gameMode = gm;

			Rectangle recPlayer1;
			recPlayer1.x = player1StartPosition.x;
			recPlayer1.y = player1StartPosition.y;
			recPlayer1.height = player::playerPaddleHeight;
			recPlayer1.width = player::playerPaddleWidth;

			Rectangle recPlayer2;
			recPlayer2.x = player2StartPosition.x;
			recPlayer2.y = player2StartPosition.y;
			recPlayer2.height = player::playerPaddleHeight;
			recPlayer2.width = player::playerPaddleWidth;

			Rectangle recItem;
			recItem.x = 0;
			recItem.y = 0;
			recItem.height = (float)item::itemHeight;
			recItem.width = (float)item::itemWidth;

			player1 = new player::Player(recPlayer1, game_setting::player1PaddleColor, game_setting::speedPaddle, 0, 0, 0);
			player2 = new player::Player(recPlayer2, game_setting::player2PaddleColor, game_setting::speedPaddle, 0, 0, 0);
			item = new item::Item(recItem, 0, false, false, (item::ItemPower)1);
			ball = new ball::Ball(ballStartPosition, ballRadiusCircle, 0, WHITE, game_setting::speedBall, ball::SPEED::NORMAL, false, false, (ball::DIRECTION)GetRandomValue(1, 4));

			StopMusicStream(audio::menuMusic);
			PlayMusicStream(audio::gameplayMusic);
		}

		void Update()
		{
			if (startGame)
			{
				switch (gameMode)
				{
				case GAME_MODE::JcJ: ModeJcJ();
					break;
				case GAME_MODE::JcAI: ModeJcIA();
					break;
				default:
					break;
				}

				ball->Move();
				ball->Collision(game::screenHeight, player1, player2);
				if (ball->Goal(game::screenWidth, player1, player2))
				{
					player1->ResetPosition(player1StartPosition);
					player2->ResetPosition(player2StartPosition);
					ball->Reset(ballStartPosition);
					item->DisablePower(player1, player2);
					item->Reset();
					startGame = false;
					PlaySound(audio::scoreSound);
				}

				if (item->PutItem(itemCoolDown))
				{
					item->StartItemValues(game::screenHeight, game::screenWidth);
				}
				if (item->CollisionBall(ball))
				{
					item->ActivePower(player1, player2, ball, game::screenWidth, game::screenHeight);
				}
				if (item->Disable(itemDuration))
				{
					item->DisablePower(player1, player2);
				}
				item->LoopPower(player1, player2, ball);

				CheckWinPlayer();
				frameCounter++;
			}
			else
			{
				if (IsKeyPressed(input::start))
				{
					startGame = true;
				}
			}
			
			if (input::CheckPause())
			{
				pause::Init();
				game::gameStatus = game::GAME::PAUSE;
			}

			UpdateMusicStream(audio::gameplayMusic);
		}

		void Draw()
		{
			DrawLine((game::screenWidth / 2), 0, (game::screenWidth / 2), game::screenHeight, WHITE);
			player1->ShowVisualPlayer();
			player2->ShowVisualPlayer();
			ball->ShowVisualBall();
			item->ShowVisualItem();

			int posY = 3;
			int font = 20;
			Color color = LIGHTGRAY;

			DrawText(TextFormat("%i", player1->GetScore()), (((game::screenWidth / 2) - 20) - (MeasureText(TextFormat("%i", player1->GetScore()), font) / 2)), posY, font, color);
			DrawText(TextFormat("%i", player2->GetScore()), (((game::screenWidth / 2) + 20) - (MeasureText(TextFormat("%i", player2->GetScore()), font) / 2)), posY, font, color);
			DrawText(TextFormat("W: %i / L: %i", player1->GetRoundsWon(), player1->GetRoundsLost()), ((game::screenWidth / 4) - (MeasureText(TextFormat("W: %i / L: %i", player1->GetRoundsWon(), player1->GetRoundsLost()), font) / 2)), posY, font, color);
			DrawText(TextFormat("W: %i / L: %i", player2->GetRoundsWon(), player2->GetRoundsLost()), ((game::screenWidth * 3 / 4) - (MeasureText(TextFormat("W: %i / L: %i", player2->GetRoundsWon(), player2->GetRoundsLost()), font) / 2)), posY, font, color);

			if (!startGame)
			{
				const char* text = "PRESS SPACE FOR START";
				font = 30;

				DrawText(text, ((game::screenWidth / 2) - MeasureText(text, font) / 2), ((game::screenHeight * 3 / 4) - (font / 2)), font, color);
			}

			item->ShowVisualPower();
			item->ShowVisualPowerDescription(game::screenWidth, game::screenHeight, powerDescriptionDuration);
		}

		void DeInit()
		{
			delete player1;
			delete player2;
			delete ball;
			delete item;

			StopMusicStream(audio::gameplayMusic);
		}

		void CheckWinPlayer()
		{
			if (player1->GetScore() == game_setting::winScore || player2->GetScore() == game_setting::winScore)
			{
				int winPlayer = 0;

				if (player1->GetScore() == game_setting::winScore)
				{
					player1->SetRoundsWon(player1->GetRoundsWon() + 1);
					player2->SetRoundsLost(player2->GetRoundsLost() + 1);
					winPlayer = 1;
				}
				else
				{
					player2->SetRoundsWon(player2->GetRoundsWon() + 1);
					player1->SetRoundsLost(player1->GetRoundsLost() + 1);
					winPlayer = 2;
				}

				win_player::Init(winPlayer, player1->GetScore(), player2->GetScore());
				game::gameStatus = game::GAME::WIN_PLAYER;
			}
		}

		void ModeJcJ()
		{
			MovePlayer1();
			MovePlayer2();
		}

		void ModeJcIA()
		{
			MovePlayer1();
			MoveIA();
		}

		void MovePlayer1()
		{
			if (IsKeyDown(input::moveUpPlayer1))
			{
				player1->Move(game::screenHeight, player::MOVE::UP);
			}
			if (IsKeyDown(input::moveDownPlayer1))
			{
				player1->Move(game::screenHeight, player::MOVE::DOWN);
			}
		}

		void MovePlayer2()
		{
			if (IsKeyDown(input::moveUpPlayer2))
			{
				player2->Move(game::screenHeight, player::MOVE::UP);
			}
			if (IsKeyDown(input::moveDownPlayer2))
			{
				player2->Move(game::screenHeight, player::MOVE::DOWN);
			}
		}

		void MoveIA()
		{
			if ((frameCounter >= 10 && frameCounter <= 30))
			{
				if ((player2->GetRec().y + (player2->GetRec().height / 2)) < ball->GetPosition().y)
				{
					player2->Move(game::screenHeight, player::MOVE::DOWN);
				}
				if ((player2->GetRec().y + (player2->GetRec().height / 2)) > ball->GetPosition().y)
				{
					player2->Move(game::screenHeight, player::MOVE::UP);
				}
			}
			else
			{
				if (frameCounter > 30)
				{
					frameCounter = 0;
				}
			}
		}

		void ResetGame()
		{
			player1->ResetRound(player1StartPosition);
			player2->ResetRound(player2StartPosition);
			ball->Reset(ballStartPosition);
			item->Reset();
			startGame = false;
		}
	}
}