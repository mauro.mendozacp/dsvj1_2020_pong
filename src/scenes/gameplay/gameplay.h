#pragma once
#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"
#include "entities\player\player.h"
#include "entities\ball\ball.h"
#include "entities\item\item.h"

namespace pong
{
	namespace gameplay
	{
		enum class GAME_MODE
		{
			JcJ = 1,
			JcAI
		};

		extern Vector2 player1StartPosition;
		extern Vector2 player2StartPosition;
		extern Vector2 ballStartPosition;

		extern player::Player *player1;
		extern player::Player *player2;
		extern ball::Ball *ball;
		extern item::Item *item;

		extern bool startGame;
		extern GAME_MODE gameMode;

		extern int itemCoolDown;
		extern int itemDuration;
		extern int powerDescriptionDuration;

		void Init(GAME_MODE gm);
		void Update();
		void Draw();
		void DeInit();


		void CheckWinPlayer();
		void ModeJcJ();
		void ModeJcIA();
		void MovePlayer1();
		void MovePlayer2();
		void MoveIA();
		void ResetGame();
	}
}

#endif // !GAMEPLAY_H
