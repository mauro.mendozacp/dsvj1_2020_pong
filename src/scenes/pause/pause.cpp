#include "pause.h"
#include "game/game.h"

namespace pong
{
	namespace pause
	{
		option::Option *option;
		const Vector2 optionStartPosition = { ((float)game::screenWidth * 3 / 8), ((float)game::screenHeight / 2) };

		void Init()
		{
			PauseMusicStream(audio::gameplayMusic);
			option = new option::Option(optionStartPosition, (int)PAUSE_MENU::CONTINUE, 30, char(62));
		}

		void Update()
		{
			MoveOption();
			if (input::CheckAccept())
			{
				switch ((PAUSE_MENU)option->GetOption())
				{
				case PAUSE_MENU::CONTINUE:
					game::gameStatus = game::GAME::INGAME;

					break;
				case PAUSE_MENU::GO_BACK:
					gameplay::DeInit();
					main_menu::Init();
					game::gameStatus = game::GAME::MAIN_MENU;

					break;
				default:
					DeInit();
					Init();
					game::gameStatus = game::GAME::PAUSE;
					return;
				}

				DeInit();
			}
		}

		void Draw()
		{
			const char* text = "PAUSE";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = 50;
			Color color = LIGHTGRAY;

			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, LIGHTGRAY);

			option->ShowVisualOption();
			posY = (int)optionStartPosition.y;
			font = 20;

			DrawText("CONTINUE", (posX - (MeasureText("CONTINUE", font) / 2)), posY, font, color);
			DrawText("GO BACK", (posX - (MeasureText("GO BACK", font) / 2)), (posY + option->GetSpeed()), font, color);
		}

		void DeInit()
		{
			delete option;
			ResumeMusicStream(audio::gameplayMusic);
		}

		void MoveOption()
		{
			if (input::CheckMoveUpDown())
			{
				int auxOption = option->GetNewOption();

				if (CheckOption(auxOption))
				{
					option->Move(auxOption);
				}
			}
		}

		bool CheckOption(int iOption)
		{
			switch ((PAUSE_MENU)(option->GetOption() + iOption))
			{
			case PAUSE_MENU::CONTINUE:
			case PAUSE_MENU::GO_BACK:
				return true;
			default:
				return false;
			}

			return false;
		}
	}
}