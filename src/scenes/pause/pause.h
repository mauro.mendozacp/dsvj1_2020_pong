#pragma once
#ifndef PAUSE_H
#define PAUSE_H

#include "raylib.h"
#include "entities/option/option.h"

namespace pong
{
	namespace pause
	{
		enum class PAUSE_MENU
		{
			CONTINUE = 1,
			GO_BACK
		};

		extern option::Option *option;
		extern const Vector2 optionStartPosition;

		void Init();
		void Update();
		void Draw();
		void DeInit();

		void MoveOption();
		bool CheckOption(int iOption);
	}
}

#endif // !PAUSE_H
