#pragma once
#ifndef PLAY_MENU_H
#define PLAY_MENU_H

#include "raylib.h"
#include "entities/option/option.h"

namespace pong
{
	namespace play_menu
	{
		enum class PLAY_MENU
		{
			JcJ = 1,
			JcIA,
			GAME_SETTING,
			GO_BACK
		};

		extern option::Option *option;
		extern const Vector2 optionStartPosition;

		void Init();
		void Update();
		void Draw();
		void DeInit();

		void MoveOption();
		bool CheckOption(int iOption);
	}
}

#endif // !PLAY_MENU_H
