#include "play_menu.h"
#include "game/game.h"

namespace pong
{
	namespace play_menu
	{
		option::Option *option;
		const Vector2 optionStartPosition = { ((float)game::screenWidth * 3 / 8), ((float)game::screenHeight / 2) };

		void Init()
		{
			option = new option::Option(optionStartPosition, (int)PLAY_MENU::JcJ, 30, char(62));
		}

		void Update()
		{
			MoveOption();
			if (input::CheckAccept())
			{
				switch ((PLAY_MENU)option->GetOption())
				{
				case PLAY_MENU::JcJ:
					gameplay::Init(gameplay::GAME_MODE::JcJ);
					game::gameStatus = game::GAME::INGAME;

					break;
				case PLAY_MENU::JcIA:
					gameplay::Init(gameplay::GAME_MODE::JcAI);
					game::gameStatus = game::GAME::INGAME;

					break;
				case PLAY_MENU::GAME_SETTING:
					game_setting::Init();
					game::gameStatus = game::GAME::GAME_SETTING;

					break;
				case PLAY_MENU::GO_BACK:
					main_menu::Init();
					game::gameStatus = game::GAME::MAIN_MENU;

					break;
				default:
					DeInit();
					Init();
					game::gameStatus = game::GAME::PLAY;
					return;

					break;
				}

				DeInit();
			}

			UpdateMusicStream(audio::menuMusic);
		}

		void Draw()
		{
			//Title
			const char* text = "PONG";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = 50;
			Color color = LIGHTGRAY;

			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, LIGHTGRAY);

			//Options
			option->ShowVisualOption();
			posY = (int)optionStartPosition.y;
			font = 20;

			DrawText("JcJ", (posX - (MeasureText("JcJ", font) / 2)), posY, font, color);
			DrawText("JcIA", (posX - (MeasureText("JcIA", font) / 2)), (posY + option->GetSpeed()), font, color);
			DrawText("GAME SETTINGS", (posX - (MeasureText("GAME SETTINGS", font) / 2)), (posY + (option->GetSpeed() * 2)), font, color);
			DrawText("BACK TO MENU", (posX - (MeasureText("BACK TO MENU", font) / 2)), (posY + (option->GetSpeed() * 3)), font, color);
		}

		void DeInit()
		{
			delete option;
		}

		void MoveOption()
		{
			if (input::CheckMoveUpDown())
			{
				int auxOption = option->GetNewOption();

				if (CheckOption(auxOption))
				{
					option->Move(auxOption);
				}
			}
		}

		bool CheckOption(int iOption)
		{
			switch ((PLAY_MENU)(option->GetOption() + iOption))
			{
			case PLAY_MENU::JcJ:
			case PLAY_MENU::JcIA:
			case PLAY_MENU::GAME_SETTING:
			case PLAY_MENU::GO_BACK:
				return true;
			default:
				return false;
			}

			return false;
		}
	}
}