#include "win_player.h"
#include "game/game.h"

namespace pong
{
	namespace win_player
	{
		int winPlayer = 0;
		int player1Score = 0;
		int player2Score = 0;
		option::Option *option;
		const Vector2 optionStartPosition = { ((float)game::screenWidth * 3 / 8), ((float)game::screenHeight / 2) };

		void Init(int wPlayer, int p1Score, int p2Score)
		{
			option = new option::Option(optionStartPosition, (int)WINPLAYER_MENU::PLAY_AGAIN, 30, char(62));
			winPlayer = wPlayer;
			player1Score = p1Score;
			player2Score = p2Score;
		}

		void Update()
		{
			MoveOption();
			if (input::CheckAccept())
			{
				switch ((WINPLAYER_MENU)option->GetOption())
				{
				case WINPLAYER_MENU::PLAY_AGAIN:
					StopMusicStream(audio::gameplayMusic);
					PlayMusicStream(audio::gameplayMusic);
					gameplay::ResetGame();
					game::gameStatus = game::GAME::INGAME;

					break;
				case WINPLAYER_MENU::GO_BACK:
					gameplay::DeInit();
					main_menu::Init();
					game::gameStatus = game::GAME::MAIN_MENU;

					break;
				default:
					DeInit();
					Init(0, 0, 0);
					game::gameStatus = game::GAME::WIN_PLAYER;
					return;
				}

				DeInit();
			}
		}

		void Draw()
		{
			const char* text = TextFormat("PLAYER %i WON!", winPlayer);
			int posX = (game::screenWidth / 2);
			int posY = (game::screenHeight / 4);
			int font = 50;
			Color color;
			if (winPlayer == 1)
			{
				color = game_setting::player1PaddleColor;
			}
			else
			{
				color = game_setting::player2PaddleColor;
			}

			DrawText(TextFormat("PLAYER %i WON!", winPlayer), (posX - (MeasureText(TextFormat("PLAYER %i WON!", winPlayer), font) / 2)), (posY - (font / 2)), font, color);

			posY = (game::screenHeight * 3 / 8);
			font = 30;
			color = LIGHTGRAY;
			DrawText(TextFormat("%i - %i", player1Score, player2Score), (posX - (MeasureText(TextFormat("%i - %i", player1Score, player2Score), font) / 2)), (posY - (font / 2)), font, color);

			option->ShowVisualOption();
			posY = (int)optionStartPosition.y;
			font = 20;

			DrawText("PLAY AGAIN", (posX - (MeasureText("PLAY AGAIN", font) / 2)), posY, font, color);
			DrawText("GO BACK", (posX - (MeasureText("GO BACK", font) / 2)), (posY + option->GetSpeed()), font, color);
		}

		void DeInit()
		{
			delete option;
			winPlayer = 0;
			player1Score = 0;
			player2Score = 0;
		}

		void MoveOption()
		{
			if (input::CheckMoveUpDown())
			{
				int auxOption = option->GetNewOption();

				if (CheckOption(auxOption))
				{
					option->Move(auxOption);
				}
			}
		}

		bool CheckOption(int iOption)
		{
			switch ((WINPLAYER_MENU)(option->GetOption() + iOption))
			{
			case WINPLAYER_MENU::PLAY_AGAIN:
			case WINPLAYER_MENU::GO_BACK:
				return true;
			default:
				return false;
			}

			return false;
		}
	}
}