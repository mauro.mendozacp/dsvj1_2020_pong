#pragma once
#ifndef WIN_PLAYER_H
#define WIN_PLAYER_H

#include "raylib.h"
#include "entities/option/option.h"

namespace pong
{
	namespace win_player
	{
		enum class WINPLAYER_MENU
		{
			PLAY_AGAIN = 1,
			GO_BACK
		};

		extern int winPlayer;
		extern option::Option *option;
		extern const Vector2 optionStartPosition;

		void Init(int wPlayer, int p1Score, int p2Score);
		void Update();
		void Draw();
		void DeInit();

		void MoveOption();
		bool CheckOption(int iOption);
	}
}

#endif // !WIN_PLAYER_H
