#pragma once
#ifndef GAME_SETTING_H
#define GAME_SETTING_H

#include "raylib.h"
#include "entities/option/option.h"

namespace pong
{
	namespace game_setting
	{
		enum class GAME_SETTING_MENU
		{
			PADDLE1_COLOR = 1,
			PADDLE2_COLOR,
			SPEED_PADDLE,
			SPEED_BALL,
			WIN_SCORE,
			GO_BACK
		};

		extern option::Option *option;
		extern const Vector2 optionStartPosition;
		extern Color player1PaddleColor;
		extern Color player2PaddleColor;
		extern float speedPaddle;
		extern float speedBall;
		extern int winScore;

		void Init();
		void Update();
		void Draw();
		void DeInit();

		void MoveOption();
		bool CheckOption(int iOption);
		void MoveValue();
		int GetPaddleColorPlayerIndex(Color color);
		Color GetPaddleColorPlayer(int actualIndex, int index, Color oppsiteColor);
	}
}

#endif // !GAME_SETTING_H
