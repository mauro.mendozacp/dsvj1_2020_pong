#include "game_setting.h"
#include "game/game.h"

namespace pong
{
	namespace game_setting
	{
		option::Option *option;
		const Vector2 optionStartPosition = { (float)(game::screenWidth / 8), (float)(game::screenHeight / 2) };
		Color player1PaddleColor = RED;
		Color player2PaddleColor = BLUE;
		
		const float speedPaddleMin = 1;
		const float speedPaddleMax = 10;
		float speedPaddle = 5;

		const float speedBallMin = 1;
		const float speedBallMax = 10;
		float speedBall = 5;

		const int winScoreMin = 1;
		const int winScoreMax = 25;
		int winScore = 3;

		const int colorLength = 6;
		Color paddleColors[colorLength] = { RED, BLUE, YELLOW, GREEN, ORANGE, PURPLE };

		void Init()
		{
			option = new option::Option(optionStartPosition, (int)GAME_SETTING_MENU::PADDLE1_COLOR, 30, char(62));
		}

		void Update()
		{
			MoveOption();
			MoveValue();
			if (input::CheckAccept())
			{
				if ((GAME_SETTING_MENU)option->GetOption() == GAME_SETTING_MENU::GO_BACK)
				{
					DeInit();
					play_menu::Init();
					game::gameStatus = game::GAME::PLAY;
				}
			}

			UpdateMusicStream(audio::menuMusic);
		}

		void Draw()
		{
			//Title
			const char* text = "GAME SETTINGS";
			int posX = (game::screenWidth / 2);
			int posY = (game::screenHeight / 4);
			int font = 50;
			Color color = LIGHTGRAY;

			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, LIGHTGRAY);

			//Options
			option->ShowVisualOption();
			font = 20;
			posX = (int)optionStartPosition.x + font;
			posY = (int)optionStartPosition.y;

			DrawText("PADDLE 1 COLOR", posX, posY, font, color);
			DrawText("PADDLE 2 COLOR", posX, (posY + option->GetSpeed()), font, color);
			DrawText("SPEED PADDLE", posX, (posY + option->GetSpeed() * 2), font, color);
			DrawText("SPEED BALL", posX, (posY + option->GetSpeed() * 3), font, color);
			DrawText("WIN SCORE", posX, (posY + option->GetSpeed() * 4), font, color);
			DrawText("BACK", posX, (posY + (option->GetSpeed() * 5)), font, color);

			//Colors
			posX = (game::screenWidth * 3 / 4);
			posY = (int)optionStartPosition.y;
			int width = 50;
			int height = 20;

			DrawRectangle(posX, posY, width, height, player1PaddleColor);
			DrawRectangle(posX, (posY + option->GetSpeed()), width, height, player2PaddleColor);

			posX = (game::screenWidth * 3 / 4) + (width / 2);
			DrawText(TextFormat("%0.f", speedPaddle), (posX - (MeasureText(TextFormat("%i", speedPaddle), font) / 2)), (posY + option->GetSpeed() * 2), font, color);
			DrawText(TextFormat("%0.f", speedBall), (posX - (MeasureText(TextFormat("%i", speedBall), font) / 2)), (posY + option->GetSpeed() * 3), font, color);
			DrawText(TextFormat("%i", winScore), (posX - (MeasureText(TextFormat("%i", winScore), font) / 2)), (posY + option->GetSpeed() * 4), font, color);

			posX = (game::screenWidth * 3 / 4);
			if ((GAME_SETTING_MENU)option->GetOption() != GAME_SETTING_MENU::GO_BACK)
			{
				posY += option->GetSpeed() * (option->GetOption() - 1);
				DrawText("<", (posX - 20), posY, font, color);
				DrawText(">", (posX + width + 10), posY, font, color);
			}
		}

		void DeInit()
		{
			delete option;
		}

		void MoveOption()
		{
			if (input::CheckMoveUpDown())
			{
				int auxOption = option->GetNewOption();

				if (CheckOption(auxOption))
				{
					option->Move(auxOption);
				}
			}
		}

		bool CheckOption(int iOption)
		{
			switch ((GAME_SETTING_MENU)(option->GetOption() + iOption))
			{
			case GAME_SETTING_MENU::PADDLE1_COLOR:
			case GAME_SETTING_MENU::PADDLE2_COLOR:
			case GAME_SETTING_MENU::SPEED_PADDLE:
			case GAME_SETTING_MENU::SPEED_BALL:
			case GAME_SETTING_MENU::WIN_SCORE:
			case GAME_SETTING_MENU::GO_BACK:
				return true;
			default:
				return false;
			}

			return false;
		}

		void MoveValue()
		{
			if (input::CheckMoveLeftRight())
			{
				int value = 0;
				if (IsKeyPressed(input::moveRight))
				{
					value = 1;
				}
				if (IsKeyPressed(input::moveLeft))
				{
					value = -1;
				}

				switch ((GAME_SETTING_MENU)option->GetOption())
				{
				case GAME_SETTING_MENU::PADDLE1_COLOR:
					player1PaddleColor = GetPaddleColorPlayer((GetPaddleColorPlayerIndex(player1PaddleColor) + value), value, player2PaddleColor);
					break;
				case GAME_SETTING_MENU::PADDLE2_COLOR:
					player2PaddleColor = GetPaddleColorPlayer((GetPaddleColorPlayerIndex(player2PaddleColor) + value), value, player1PaddleColor);
					break;
				case GAME_SETTING_MENU::SPEED_PADDLE:
					if (speedPaddleMin <= (speedPaddle + value) && speedPaddleMax >= (speedPaddle + value))
					{
						speedPaddle += value;
					}
					break;
				case GAME_SETTING_MENU::SPEED_BALL:
					if (speedBallMin <= (speedBall + value) && speedBallMax >= (speedBall + value))
					{
						speedBall += value;
					}
					break;
				case GAME_SETTING_MENU::WIN_SCORE:
					if (winScoreMin <= (winScore + value) && winScoreMax >= (winScore + value))
					{
						winScore += value;
					}
					break;
				default:
					break;
				}
			}
		}

		int GetPaddleColorPlayerIndex(Color color)
		{
			for (int i = 0; i < colorLength; i++)
			{
				if (ColorToInt(color) == ColorToInt(paddleColors[i]))
				{
					return i;
				}
			}

			return -1;
		}

		Color GetPaddleColorPlayer(int actualIndex, int index, Color oppsiteColor)
		{
			int auxIndex = actualIndex;
			Color auxColor;

			if (auxIndex < 0)
			{
				auxColor = paddleColors[colorLength - 1];
				if (ColorToInt(auxColor) == ColorToInt(oppsiteColor))
				{
					auxIndex = colorLength - 2;
					return GetPaddleColorPlayer(auxIndex, index, oppsiteColor);
				}

				return auxColor;
			}
			if (auxIndex > colorLength - 1)
			{
				auxColor = paddleColors[0];
				if (ColorToInt(auxColor) == ColorToInt(oppsiteColor))
				{
					auxIndex = 1;
					return GetPaddleColorPlayer(auxIndex, index, oppsiteColor);
				}

				return auxColor;
			}

			auxColor = paddleColors[auxIndex];
			if (ColorToInt(auxColor) == ColorToInt(oppsiteColor))
			{
				auxIndex += index;
				return GetPaddleColorPlayer(auxIndex, index, oppsiteColor);
			}

			return auxColor;
		}
	}
}