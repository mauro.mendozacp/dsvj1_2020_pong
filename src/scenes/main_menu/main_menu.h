#pragma once
#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "raylib.h"
#include "entities/option/option.h"

namespace pong
{
	namespace main_menu
	{
		enum class MAIN_MENU
		{
			PLAY = 1,
			CONTROLS,
			CREDITS,
			EXIT
		};

		extern option::Option *option;
		extern const Vector2 optionStartPosition;

		void Init();
		void Update();
		void Draw();
		void DeInit();

		void MoveOption();
		bool CheckOption(int iOption);
	}
}

#endif // !MAIN_MENU_H