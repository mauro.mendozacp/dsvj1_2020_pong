#include "main_menu.h"
#include "game/game.h"

namespace pong
{
	namespace main_menu
	{
		option::Option *option;
		const Vector2 optionStartPosition = { ((float)game::screenWidth * 3 / 8), ((float)game::screenHeight / 2) };

		void Init()
		{
			option = new option::Option(optionStartPosition, (int)MAIN_MENU::PLAY, 30, char(62));
			
			if (!IsMusicPlaying(audio::menuMusic))
			{
				PlayMusicStream(audio::menuMusic);
			}
		}

		void Update()
		{
			MoveOption();
			if (input::CheckAccept())
			{
				switch ((MAIN_MENU)option->GetOption())
				{
				case MAIN_MENU::PLAY:
					play_menu::Init();
					game::gameStatus = game::GAME::PLAY;

					break;
				case MAIN_MENU::CONTROLS:
					controllers::Init();
					game::gameStatus = game::GAME::CONTROLS;

					break;
				case MAIN_MENU::CREDITS:
					credits::Init();
					game::gameStatus = game::GAME::CREDITS;

					break;
				case MAIN_MENU::EXIT:
					game::gameStatus = game::GAME::EXIT;

					break;
				default:
					DeInit();
					Init();
					game::gameStatus = game::GAME::MAIN_MENU;
					return;
				}

				DeInit();
			}

			UpdateMusicStream(audio::menuMusic);
		}

		void Draw()
		{
			const char* text = "PONG";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = 50;
			Color color = LIGHTGRAY;

			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, LIGHTGRAY);

			option->ShowVisualOption();
			posY = (int)optionStartPosition.y;
			font = 20;

			DrawText("PLAY", (posX - (MeasureText("PLAY", font) / 2)), posY, font, color);
			DrawText("CONTROLS", (posX - (MeasureText("CONTROLS", font) / 2)), (posY + option->GetSpeed()), font, color);
			DrawText("CREDITS", (posX - (MeasureText("CREDITS", font) / 2)), (posY + option->GetSpeed() * 2), font, color);
			DrawText("EXIT", (posX - (MeasureText("EXIT", font) / 2)), (posY + option->GetSpeed() * 3), font, color);
		}

		void DeInit()
		{
			delete option;
		}

		void MoveOption()
		{
			if (input::CheckMoveUpDown())
			{
				int auxOption = option->GetNewOption();

				if (CheckOption(auxOption))
				{
					option->Move(auxOption);
				}
			}
		}

		bool CheckOption(int iOption)
		{
			switch ((MAIN_MENU)(option->GetOption() + iOption))
			{
			case MAIN_MENU::PLAY:
			case MAIN_MENU::CONTROLS:
			case MAIN_MENU::CREDITS:
			case MAIN_MENU::EXIT:
				return true;
			default:
				return false;
			}

			return false;
		}
	}
}