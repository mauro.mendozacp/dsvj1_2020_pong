#pragma once
#include "raylib.h"
#include <iostream>
#include <string>
using namespace std;

namespace pong
{
	namespace input
	{
		extern int inputKey;

		extern int moveUpPlayer1;
		extern int moveDownPlayer1;
		extern int moveUpPlayer2;
		extern int moveDownPlayer2;
		extern int pause;

		extern const int moveUp;
		extern const int moveDown;
		extern const int moveRight;
		extern const int moveLeft;
		extern const int start;
		extern const int accept;

		int GetInputChange();
		const char* GetInputText(int iKey);
		bool CheckMoveUpDown();
		bool CheckMoveLeftRight();
		bool CheckPause();
		bool CheckAccept();
	}
}