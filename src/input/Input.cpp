#include "input.h"

namespace pong
{
	namespace input
	{
		int inputKey = 0;

		int moveUpPlayer1 = KEY_UP;
		int moveDownPlayer1 = KEY_DOWN;
		int moveUpPlayer2 = KEY_W;
		int moveDownPlayer2 = KEY_S;
		int pause = KEY_P;

		const int moveUp = KEY_UP;
		const int moveDown = KEY_DOWN;
		const int moveRight = KEY_RIGHT;
		const int moveLeft = KEY_LEFT;
		const int start = KEY_SPACE;
		const int accept = KEY_ENTER;

		int GetInputChange()
		{
			inputKey = GetKeyPressed();

			if ((inputKey >= 32) && (inputKey <= 125))
			{
				if (inputKey >= 97 && inputKey <= 122)
				{
					inputKey -= 32;
				}
				return inputKey;
			}
			if (IsKeyPressed(moveUp))
			{
				return moveUp;
			}
			if (IsKeyPressed(moveDown))
			{
				return moveDown;
			}
			if (IsKeyPressed(moveRight))
			{
				return moveRight;
			}
			if (IsKeyPressed(moveLeft))
			{
				return moveLeft;
			}

			return 0;
		}

		const char* GetInputText(int iKey)
		{
			if ((iKey >= 32) && (iKey <= 125))
			{
				char *letter = new char;
				letter[0] = (char)iKey;
				letter[1] = '\0';
				return letter;
			}
			if (iKey == moveUp)
			{
				return "ARROW UP";
			}
			if (iKey == moveDown)
			{
				return "ARROW DOWN";
			}
			if (iKey == moveLeft)
			{
				return "ARROW LEFT";
			}
			if (iKey == moveRight)
			{
				return "ARROW RIGHT";
			}

			return " ";
		}
		bool CheckMoveUpDown()
		{
			if (IsKeyPressed(moveUp) || IsKeyPressed(moveDown))
			{
				return true;
			}
			return false;
		}
		bool CheckMoveLeftRight()
		{
			if (IsKeyPressed(moveLeft) || IsKeyPressed(moveRight))
			{
				return true;
			}
			return false;
		}
		bool CheckPause()
		{
			if (IsKeyPressed(pause))
			{
				return true;
			}
			return false;
		}
		bool CheckAccept()
		{
			if (IsKeyPressed(accept))
			{
				return true;
			}
			return false;
		}
	}
}