#include "game.h"

namespace pong
{
	namespace game
	{
		//Screen Limits
		const int screenWidth = 800;
		const int screenHeight = 450;
		const int frames = 60;

		GAME gameStatus = GAME::MAIN_MENU;

		void RunGame()
		{
			Init();

			// Main game loop
			while (!WindowShouldClose() && gameStatus != GAME::EXIT)
			{
				Update();
				Draw();
			}
			CloseWindow();
		}

		void Init()
		{
			audio::Init();
			main_menu::Init();
			gameStatus = GAME::MAIN_MENU;
			InitWindow(screenWidth, screenHeight, "PONG");

			// Set our game to run at 60 frames-per-second
			SetTargetFPS(frames);
		}

		void Update()
		{
			// Update
			switch (gameStatus)
			{
			case GAME::INGAME:
				gameplay::Update();

				break;
			case GAME::MAIN_MENU:
				main_menu::Update();

				break;
			case GAME::PAUSE:
				pause::Update();

				break;
			case GAME::WIN_PLAYER:
				win_player::Update();

				break;
			case GAME::PLAY:
				play_menu::Update();

				break;
			case GAME::GAME_SETTING:
				game_setting::Update();

				break;

			case GAME::CREDITS:
				credits::Update();

				break;

			case GAME::CONTROLS:
				controllers::Update();

				break;
			default:
				gameStatus = GAME::EXIT;

				break;
			}
		}

		void Draw()
		{
			BeginDrawing();

			ClearBackground(BLACK);

			switch (gameStatus)
			{
			case GAME::INGAME:
				gameplay::Draw();

				break;
			case GAME::MAIN_MENU:
				main_menu::Draw();

				break;
			case GAME::PAUSE:
				pause::Draw();

				break;
			case GAME::WIN_PLAYER:
				win_player::Draw();

				break;
			case GAME::CONTROLS:
				controllers::Draw();

				break;
			case GAME::CREDITS:
				credits::Draw();

				break;
			case GAME::PLAY:
				play_menu::Draw();

				break;

			case GAME::GAME_SETTING:
				game_setting::Draw();

				break;
			default:
				gameStatus = GAME::EXIT;

				break;
			}

			EndDrawing();
		}

		void DeInit()
		{
			audio::DeInit();
		}
	}
}