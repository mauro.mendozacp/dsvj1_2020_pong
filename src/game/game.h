#pragma once
#ifndef GAME_H
#define GAME_H

#include "scenes/gameplay/gameplay.h"
#include "scenes/main_menu/main_menu.h"
#include "scenes/play_menu/play_menu.h"
#include "scenes/win_player/win_player.h"
#include "scenes/controllers/controllers.h"
#include "scenes/credits/credits.h"
#include "scenes/game_setting/game_setting.h"
#include "scenes/pause/pause.h"
#include "audio/audio.h"
#include "raylib.h"
#include <iostream>

using namespace std;

namespace pong
{
	namespace game
	{
		enum class GAME
		{
			MAIN_MENU = 1,
			INGAME,
			PLAY,
			CONTROLS,
			CREDITS,
			GAME_SETTING,
			PAUSE,
			WIN_PLAYER,
			EXIT
		};

		extern const int screenWidth;
		extern const int screenHeight;
		extern const int frames;

		extern GAME gameStatus;

		void RunGame();

		void Init();
		void Update();
		void Draw();
		void DeInit();
	}
}

#endif // !GAME_H